#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define fo(i,b,n) for(int i=(b); i<(n) ; i++)
#define xx first
#define yy second
#define pb push_back
#define sz(n) int(n.size())

#define pii pair
#define MP make_pair

#define tp tuple
#define MT make_tuple
#define gt(x,y) get<x>(y)

#define read(x) scanf("%d",&x)
#define read2(x,y) scanf("%d%d",&x,&y)
#define readl(x) scanf("%lld",&x)
#define readl2(x,y) scanf("%lld%lld",&x,&y)
#define readd(x) scanf("%lf",&x)
#define readd2(x,y) scanf("%lf%lf",&x,&y)

#define pr(n) printf("%d",n)
#define prn(n) printf("%d\n",n)
#define prl(n) printf("%lld",n)
#define prln(n) printf("%lld\n",n)
#define prd(x) printf("%lf",x)
#define prdn(x) printf("%lf\n",x)

#define TC(n) printf("Case %d: ",n)
const int N=1e5 + 5;
vector<int>g[N];
int low[N],d[N],vis[N],ap[N];

void dfs(int u,int par)
{
    vis[u]=1;
    static int dep=0;
    low[u]=d[u]= (++dep);
    for(int v:g[u])
    {
        if(vis[v] and par ^ v )
        {
            low[u]=min(low[u],d[v]);
        }
        if(!vis[v])
        {
            dfs(v,u);
            low[u]=min(low[u],low[v]);
            if(low[v] >= d[u] )  // ap
            {
                ap[u]=1;
            }
            if(low[v] > d[u])  //ab
            {
                printf("%d %d\n",u,v);
            }
        }
    }
    return;
}


int main()
{
    int n,m;
    read2(n,m);
    fo(i,0,m)
    {
        int x,y;
        read2(x,y);
        g[x].pb(y),g[y].pb(x);
    }
    puts("AB");
    fo(i,0,n)
    {
        if(!vis[i])dfs(i,0);
    }
    puts("AP");
    fo(i,0,n)
    {
        if(ap[i])prn(i);
    }

    return 0;
}















