#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define fo(i,b,n) for(int i=(b); i<(n) ; i++)
#define xx first
#define yy second
#define pb push_back
#define sz(n) int(n.size())

#define pii pair
#define MP make_pair

#define tp tuple
#define MT make_tuple
#define gt(x,y) get<x>(y)

#define read(x) scanf("%d",&x)
#define read2(x,y) scanf("%d%d",&x,&y)
#define readl(x) scanf("%lld",&x)
#define readl2(x,y) scanf("%lld%lld",&x,&y)
#define readd(x) scanf("%lf",&x)
#define readd2(x,y) scanf("%lf%lf",&x,&y)

#define pr(n) printf("%d",n)
#define prn(n) printf("%d\n",n)
#define prl(n) printf("%lld",n)
#define prln(n) printf("%lld\n",n)
#define prd(x) printf("%lf",x)
#define prdn(x) printf("%lf\n",x)

#define TC(n) printf("Case %d: ",n)

const int N=1e5 + 5;
vector<int>g[N];
int vis[N],par[N],ap[N],dis[N],mi[N];


void dfs(int u)
{
    static int time=0; //for not using time as function parameter 
    dis[u]=mi[u]=++time; //discover and min time
    int child=0; //number of child node
    vis[u]=1; 
    for( int v:g[u] )
    {
        if(!vis[v])
        {
            child++;
            par[v]=u;
            dfs(v);
            mi[u]=min(mi[u],mi[v]); //min value of child and itself 
            if(par[u]==-1 and child >1 ) //first condition root + child>1
                ap[u]=1; 
            if(par[u] !=-1 and mi[v]>=dis[u]  ) //2nd condition !root + !back edges
                ap[u]=1;
        }
        else if( par[u] != v ) // !root check 
            mi[u]=min(mi[u],dis[v]); // back edges check 
    }
}


int main()
{
    int n,m;
    read2(n,m);
    //index start from 1..n
    fo(i,0,m)
    {
        int x,y;
        read2(x,y);
        g[x].pb(y);
        g[y].pb(x);
    }
    par[1]=-1;
    dfs(1);
    fo(i,1,n+1)prn(ap[i]);


    return 0;
}

/***
6 6
1 2
2 3
1 4
4 5
5 6
4 6


***/
























