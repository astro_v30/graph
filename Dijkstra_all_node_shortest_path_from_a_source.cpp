#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define fo(i,b,n) for(int i=(b); i<(n) ; i++)
#define xx first
#define yy second
#define pb push_back
#define sz(n) int(n.size())

#define pii pair
#define MP make_pair

#define tp tuple
#define MT make_tuple
#define gt(x,y) get<x>(y)

#define read(x) scanf("%d",&x)
#define read2(x,y) scanf("%d%d",&x,&y)
#define readl(x) scanf("%lld",&x)
#define readl2(x,y) scanf("%lld%lld",&x,&y)
#define readd(x) scanf("%lf",&x)
#define readd2(x,y) scanf("%lf%lf",&x,&y)

#define pr(n) printf("%d",n)
#define prn(n) printf("%d\n",n)
#define prl(n) printf("%lld",n)
#define prln(n) printf("%lld\n",n)
#define prd(x) printf("%lf",x)
#define prdn(x) printf("%lf\n",x)

#define TC(n) printf("Case %d: ",n)
/**********************************/

const int N=1e5+5;
vector< pii<int,int> >g[N];
ll dis[N];
#define inf 99999999999999999

int main()
{
    int n,m;
    read2(n,m);
    fo(i,0,m)
    {
        int x,y,w;
        read2(x,y),read(w);
        g[x].pb(MP(y,w)),g[y].pb(MP(x,w));
    }

    //all node shortest path from node 0;
    fo(i,0,N)dis[i]=inf;
    dis[0]=0ll;
    priority_queue< pii<ll,int> >q;
    q.push(MP(0ll,0));

    while(!q.empty())
    {
        auto top=q.top();
        q.pop();
        int u=top.yy;
        for( auto v : g[u] )
        {
            ll temp=dis[u]+v.yy;
            if( temp  < dis[v.xx] )
            {
                dis[v.xx]=temp;
                q.push( MP( -(temp),v.xx ) );
            }
        }
    }

    fo(i,0,n){
        printf("0 - %d > ",i);
        if(dis[i]==inf)puts("no path");
        else prln(dis[i]);
    }

    return 0;
}

/*
4 4
0 1 2
0 2 5
1 2 1
2 3 3

9 9
0 1 2
0 2 8
1 3 100
3 6 2
1 5 2
2 6 3
5 6 3
7 8 1
4 8 2


*/










