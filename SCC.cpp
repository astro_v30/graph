#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define fo(i,b,n) for(int i=(b); i<(n) ; i++)
#define xx first
#define yy second
#define pb push_back
#define sz(n) int(n.size())

#define pii pair
#define MP make_pair

#define tp tuple
#define MT make_tuple
#define gt(x,y) get<x>(y)

#define read(x) scanf("%d",&x)
#define read2(x,y) scanf("%d%d",&x,&y)
#define readl(x) scanf("%lld",&x)
#define readl2(x,y) scanf("%lld%lld",&x,&y)
#define readd(x) scanf("%lf",&x)
#define readd2(x,y) scanf("%lf%lf",&x,&y)

#define pr(n) printf("%d",n)
#define prn(n) printf("%d\n",n)
#define prl(n) printf("%lld",n)
#define prln(n) printf("%lld\n",n)
#define prd(x) printf("%lf",x)
#define prdn(x) printf("%lf\n",x)

#define TC(n) printf("Case %d: ",n)
/**********************************/

const int N=100005;
vector<int>g[N],rg[N],scc[N];
int vis[N];
vector<int>stk;
int cnt=0;

void dfs1(int u)
{
    vis[u]=1;
    for(int v: g[u]){
        if(!vis[v])dfs1(v);
    }
    stk.pb(u);
}

void dfs2(int u)
{
    vis[u]=1;
    scc[cnt].pb(u);
    for(int v: rg[u] ){
        if(!vis[v])dfs2(v);
    }
}

int main()
{
    int n,m;
    read2(n,m);
    fo(i,0,m){
        int x,y;
        read2(x,y);
        g[x].pb(y),rg[y].pb(x);
    }
    memset(vis,0,sizeof vis);
    fo(i,1,n+1){
        if(!vis[i])dfs1(i);
    }
    memset(vis,0,sizeof vis);
    cnt=0;
    for(int i=sz(stk)-1;i>=0;i--)
    {
        if(!vis[ stk[i] ] ){
            dfs2( stk[i] );
            cnt++;
        }
    }
    puts("");
    cout<<"total scc : "<< cnt<<endl;

    fo(i,0,cnt){
        for(int x: scc[i]){
            cout<<x<<" ";
        }
        cout<<endl;
    }



    return 0;
}






















